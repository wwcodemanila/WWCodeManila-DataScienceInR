# Welcome to the Data Science Study Group!

## Setting up your computer

### Windows 

First, determine whether you are running on 32- or 64-bit Windows. 

Open the Control Panel, go to System and Security and then System. This will tell 
you whether you’re on a 32-bit or 64-bit system. Note that what matters is the 
operating system, not the actual processor. See screenshot below:

<img src=https://cdn-images-1.medium.com/max/1000/1*zjfJg3xKU-1p5BNU_BrQIg.png>

#### Install `MikTeX` (pronounced _mick-tech_)

1. Go to  http://miktex.org/download
2. Install using the **Basic Installer** for Windows. 

#### Install R
1. Go to https://cran.rstudio.com/ in your web browser.
2. Click the Download R for Windows link.
1. Click the **Install R for the first time** link
1. Then click the the **Download R 3.4.3 for Windows** link to download R.
2. Run the installer to install R

#### Install RStudio

Go to the RStudio download page: https://www.rstudio.com/products/rstudio/download/

From this page, you can download various installers for RStudio. You have to choose the one for Windows. At the time of this writing, it’s called **RStudio 1.1.423 - Windows Vista/7/8/10**. You don’t have to choose between a 32-bit and 64-bit version.
When the download is complete, run the file you just downloaded. As usual, Windows may ask if want to “allow this app from an unknown publisher to make changes to your PC”. And as usual, make sure to click Yes.
